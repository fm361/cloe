% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/functions.R
\name{p_correct_m}
\alias{p_correct_m}
\title{Correct multiple probabilities for logs}
\usage{
p_correct_m(p)
}
\arguments{
\item{p}{a vector or matrix of probabilities}
}
\value{
an object corresponding to \code{p} where no entry is
  exactly 0 or 1
}
\description{
Corrects multiple values so that there are no exact zeros or ones. Entries
equal to zero are corrected to \code{.Machine$double.eps}; those equal to 
one are corrected to \code{1 - .Machine$double.eps}.
This provides safe values for \code{log(p)} and \code{log(1-p)}.
}
