% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/summaries.R
\name{summarise}
\alias{summarise}
\title{Estimate parameters}
\usage{
summarise(cm, method = "map", ...)
}
\arguments{
\item{cm}{a \code{cloe_mcmc} object. If the sampler was run with the
`save_all` option, this function will estimate parameters from the frist
chain (real posterior).}

\item{method}{a summary method. Currently only 'map' (maximum a posteriori)
is available.}

\item{...}{additional method-specific parameters}
}
\value{
a \code{cloe_summary} object with inferred parameters
}
\description{
Summarises the MCMCMC output to infer the parameters of interest: genotypes,
their phylogeny, and clonal fractions.
}
\seealso{
\code{\link{cloe_mcmc}} for \code{cm},
  \code{\link{cloe_summary}} for the return value, and
  \code{\link{summarise_map}} for the MAP summary
}
