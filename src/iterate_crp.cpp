#include <cmath>
#include <set>
#include <RcppArmadillo.h>

// [[Rcpp::depends(RcppArmadillo)]]
// [[Rcpp::plugins("cpp11")]]


#define STIRLING_APPROX_CONST_LOG 0.9189385332046727


double lbeta_approx(const double x, const double y){
  if(x < 1 || y < 1){
    return -arma::datum::inf;
  }
  return STIRLING_APPROX_CONST_LOG +
    (x - 0.5) * std::log(x) +
    (y - 0.5) * std::log(y) -
    (x + y - 0.5) * std::log(x + y);
}


// approximates log(Beta(x + a, y + b) / Beta(x, y))
// it is assumed that a, b >= 0
double lbeta_ratio_approx(
  const double x,
  const double a,
  const double y,
  const double b
){
  if(x < 1 || y < 1){
    return -arma::datum::inf;
  }
  return (x + a - 0.5) * std::log(x + a) +
    (y + b - 0.5) * std::log(y + b) -
    (x + a + y + b - 0.5) * std::log(x + a + y + b) -
    (x - 0.5) * std::log(x) -
    (y - 0.5) * std::log(y) +
    (x + y - 0.5) * std::log(x + y);
}


arma::uword next_class(const arma::uvec& nc){
  arma::uword j = 0;

  for(; j < nc.n_elem; ++j){
    if(nc[j] == 0){
      return j;
    }
  }

  return j;
}


arma::uvec count_classes(const arma::uvec& classes){
  arma::uvec ret (classes.max() + 1, arma::fill::zeros);

  for(arma::uword i = 0; i < classes.n_elem; ++i){
    ret[classes[i]]++;
  }

  return ret;
}


// this is used to compute the conditional posterior probability that
// c_i = c | c_{-i}, y_i
// for some old class c, and with phi prior = Beta(1, 1)
// Jain and Neal 2004 eq 2.4 (line 1)
double dbbinom_gibbs_old(
  const arma::mat&  x,
  const arma::mat&  d,
  const arma::mat&  y,
  const arma::uword i,
  const arma::uvec& is
){
  double ret = 0;
  
  // colsums
  arma::rowvec sx = arma::sum(x.rows(is), 0);
  arma::rowvec sy = arma::sum(y.rows(is), 0);

  for(arma::uword m = 0; m < x.n_cols; ++m){
    ret += lbeta_ratio_approx(
        sx[m] + 1,
        x.at(i, m),
        sy[m] + 1,
        y.at(i, m)
      );
  }

  return ret;
}


// this is used to compute the conditional posterior probability that
// c_i != c_j for all j != i | c_{-i}, y_i
// this is for a new class, and with phi prior = Beta(1, 1)
// Jain and Neal 2004 eq 2.4 (line 2)
double dbbinom_gibbs_new(
  const arma::mat& x,
  const arma::mat& d,
  const arma::mat& y,
  int i
){
  double ret = 0;

  for(arma::uword m = 0; m < x.n_cols; ++m){
    ret += lbeta_approx(
        x.at(i, m) + 1,
        y.at(i, m) + 1
      );
  }

  return ret;
}



// this is used to compute the likelihood of all c | y
// with phi prior = Beta(1, 1)
// [eq. 3.7 in Jain and Neal 2004]
double dbbinom_ll(
  const arma::mat& x,
  const arma::mat& d,
  const arma::mat& y,
  const arma::uvec& classes,
  const arma::uvec& nc
){
  arma::uword i, j, k;
  arma::uword m = x.n_cols;
  double ret = 0;

  arma::uvec is;
  arma::mat csx, csy;

  for(k = 0; k < nc.n_elem; ++k){
    if(nc[k] == 0) continue;

    is = arma::find(classes == k);

    // cumulative sums
    csx = arma::mat(is.n_elem, m);
    csy = arma::mat(is.n_elem, m);

    csx.row(0) = arma::rowvec(m, arma::fill::ones);
    csy.row(0) = arma::rowvec(m, arma::fill::ones);

    // process i = 0
    // the prior for phi here is simply Beta(1, 1)
    i = 0;
    for(j = 0; j < m; ++j){
      ret += lbeta_approx(
          x.at(is[i], j) + 1,
          y.at(is[i], j) + 1
        );
    }

    // compute cumsum and process other elements
    // the prior here gets updated after seeing the previous entries
    for(i = 1; i < is.n_elem; ++i){
      csx.row(i) = csx.row(i - 1) + x.row(is[i - 1]);
      csy.row(i) = csy.row(i - 1) + y.row(is[i - 1]);

      for(j = 0; j < m; ++j){
        ret += lbeta_ratio_approx(
            csx.at(i, j),
            x.at(is[i], j),
            csy.at(i, j),
            y.at(is[i], j)
          );
      }
    }
  }

  return ret;
}



// this is used to compute the likelihood of one c | y
// for the S-M move (split-merge!) with phi prior = Beta(1, 1)
// Jain and Neal 2004 eq. 3.8, 3.9
double dbbinom_sm(
  const arma::mat& x,
  const arma::mat& d,
  const arma::mat& y,
  const arma::uvec& is // rows corresponding to class c
){
  arma::uword i, j;
  arma::uword m = x.n_cols;
  double ret = 0;

  arma::mat csx, csy;

  // cumulative sums
  csx = arma::mat(is.n_elem, m);
  csy = arma::mat(is.n_elem, m);

  csx.row(0) = arma::rowvec(m, arma::fill::ones);
  csy.row(0) = arma::rowvec(m, arma::fill::ones);

  // process i = 0
  // the prior for phi here is simply Beta(1, 1)
  i = 0;
  for(j = 0; j < m; ++j){
    ret += lbeta_ratio_approx(
        csx.at(i, j),
        x.at(is[i], j),
        csy.at(i, j),
        y.at(is[i], j)
      );
  }

  // compute cumsum and process other elements
  // the prior here gets updated after seeing the previous entries
  for(i = 1; i < is.n_elem; ++i){
    csx.row(i) = csx.row(i - 1) + x.row(is[i - 1]);
    csy.row(i) = csy.row(i - 1) + y.row(is[i - 1]);

    for(j = 0; j < m; ++j){
      ret += lbeta_ratio_approx(
          csx.at(i, j),
          x.at(is[i], j),
          csy.at(i, j),
          y.at(is[i], j)
        );
    }
  }

  return ret;
}



arma::vec lnormalise(
  const arma::vec&  x, 
  const arma::uvec& range, 
  const bool        exp
){
  arma::uword max_i = x.index_max();
  arma::vec   y (x.elem(arma::find(range != max_i)));
  arma::vec   ret = x - x[max_i] - std::log1p(arma::accu(arma::exp(y - x[max_i])));

  if(exp){
    return arma::exp(ret);
  }else{
    return ret;
  }
}



arma::uword sample_int(const arma::vec& probs){
  arma::uvec is = arma::find(arma::randu<double>() <= arma::cumsum(probs));
  return is[0];
}



// sample without replacement
arma::uvec sample_ints(
  const arma::uword n,
  arma::vec probs       // copy this vector
){
  arma::uvec is (n);
  arma::uvec tmp;
  arma::vec  cum_probs;

  for(arma::uword i = 0; i < n; ++i){
    cum_probs     = arma::cumsum(probs);
    tmp           = arma::find(arma::randu<double>() <= cum_probs);
    is[i]         = tmp[0];
    probs = probs / (1.0 - probs[tmp[0]]);
    probs[tmp[0]] = 0;
  }
  
  return is;
}



// sample uniformly without replacement
arma::uvec sample_ints(
  const arma::uword n,
  const arma::uword size
){
  arma::vec probs (size);
  probs.fill(1.0 / size);

  return sample_ints(n, probs);
}



arma::uword sample_from_two(
  const arma::uword a,
  const arma::uword b,
  const arma::vec probs
){
  return arma::randu<double>() > probs[0] ? b : a;
}



arma::uvec find_minus1(
  const arma::uvec& x,
  const arma::uword n,
  const arma::uword find,
  const arma::uword skip1
){
  arma::uvec  ks (n);
  arma::uword i = 0;
  arma::uword z = 0;

  for(; i < x.n_elem; ++i){
    if(x[i] == find){
      if(i != skip1){
        ks[z++] = i;
      }
    }
  }

  if(z < n){
    ks.resize(z);
  }

  return ks;
}



double lprior_c(
  const double alpha,
  const arma::uvec& nc
){
  double ret;

  arma::uvec vs = arma::find(nc > 0);

  // Jain and Neal 2004 eq. 3.3
  ret = vs.n_elem * std::log(alpha) + 
    arma::sum(arma::lgamma(nc.elem(vs))) +
    lgamma(alpha) -
    lgamma(arma::sum(nc) + alpha);

  return ret;
}



arma::uvec fetch_all(const std::set<arma::uword>& set){
  arma::uvec  ret (set.size());
  arma::uword z = 0;

  for(auto it = set.begin(); it != set.end(); ++it){
    ret[z++] = *it;
  }

  return ret;
}



// [[Rcpp::export]]
Rcpp::List iterate_crp(
  const arma::uword iterations,
  const arma::mat&  reads,
  const arma::mat&  depths,
  const double      alpha,
  const arma::uword scans,
  arma::uvec        classes,
  Rcpp::List        traces
){
  // number of observations
  const arma::uword n = reads.n_rows;

  // current number of classes
  arma::uword k = classes.max() + 1;

  arma::uword iteration;
  arma::uword i, j;
  arma::uword k1, k2, k2_bis;
  arma::uword old_c, new_c;
  arma::uword next_c;
  arma::uword mh = 0;
  arma::uword diff;

  double ll;
  double mh_ratio;

  const double den              = n - 1 + alpha;
  const double lalpha           = std::log(alpha);
  const double new_class_lprior = lalpha - std::log(den);

  arma::vec lprobs, probs;

  const arma::mat wreads = depths - reads;
  arma::uvec to_rm;
  arma::uvec vs;

  arma::uvec range2 (2);
  range2[0] = 0;
  range2[1] = 1;

  arma::uvec rangek (k);
  for(i = 0; i < k; ++i){
    rangek[i] = i;
  }

  // this vector has one entry per class, each class storing the indices of the
  // elements belonging to that class
  std::vector<std::set<arma::uword>> indices (k);
  for(i = 0; i < n; ++i){
    indices[classes[i]].insert(i);
  }

  std::set<arma::uword> emptyset;

  // count occurrences of classes
  arma::uvec nc = count_classes(classes);
  next_c        = next_class(nc);

  // we need additional copies of these for the SM move
  arma::uvec classes_launch, classes_new;
  arma::uvec nc_launch, nc_new;

  arma::uvec obs;


  for(iteration = 0; iteration < iterations; ++iteration){
    if(iteration % 100 == 0){
      Rcpp::Rcout << "Clustering iteration " << iteration << "   \r";
    }

    /// GC: remove unused classes
    if(iteration % 20 == 1){
      to_rm = arma::find(nc == 0);

      // if anything is to be removed
      if(to_rm.n_elem > 0){
        to_rm = arma::reverse(to_rm);
        // remove class parameters (order in to_rm is descending)
        for(i = 0; i < to_rm.n_elem; ++i){
          nc.shed_row(to_rm[i]);   // these entries should be 0
          indices.erase(indices.begin() + to_rm[i]);
        }

        // update class labels
        for(i = 0; i < n; ++i){
          classes[i] -= arma::accu(classes[i] > to_rm);
        }

        // update auxiliary variables
        k      -= to_rm.n_elem;
        next_c  = next_class(nc);
      }
    }

    
    /// update cluster assignments
    for(i = 0; i < n; ++i){
      // get current class associated with observation i
      old_c = classes[i];

      // if singleton, remove cluster
      if(nc[old_c] == 1){
        // if old_c > next_c, next_c is unchanged; otherwise, next_c = old_c
        if(next_c > old_c){
          next_c = old_c;
        }
      }

      // draw c_i ~ p(c_i | c_{-i}, y_i) [eq 3.7 in Neal 2009]
      // unset class for this object
      // classes[i] = arma::datum::nan;
      nc[old_c]--;
      indices[old_c].erase(i);

      // conditional posterior for classes 0 .. k-1 k
      lprobs.set_size(k + 1);
      lprobs.fill(-arma::datum::inf);

      // old classes (0 .. k-1)
      for(j = 0; j < k; ++j){
        // only compute likelihoods for non-empty classes
        if(nc[j] == 0) continue;

        // Jain and Neal 2004, eq 2.4
        lprobs[j] = std::log(nc[j] / den) + dbbinom_gibbs_old(
          reads,
          depths,
          wreads,
          i,
          fetch_all(indices[j])
        );
      }

      // new class?
      lprobs[k] = new_class_lprior + dbbinom_gibbs_new(
        reads,
        depths,
        wreads,
        i
      );

      // want to sample classes from 0 to k
      // k+1 of them, of which k are currently in use
      diff = k + 1 - rangek.n_elem;
      if(diff > 0){
        rangek.resize(k + 1);
        for(j = k + 1 - diff; j < rangek.n_elem; j++){
          rangek[j] = j;
        }
      }else if(diff < 0){
        rangek.resize(k + 1);
      }
      new_c  = sample_int(lnormalise(lprobs, rangek, true));

      // did we choose to add a new cluster?
      if(new_c == k){ // 0-based
        // cluster id
        new_c = next_c;

        // is this a new id or are we recycling an old one?
        if(new_c < nc.n_elem){
          nc[new_c] = 1;
        }else{
          // update number of clusters
          k++;
          // new cluster has one observation
          nc.resize(nc.n_elem + 1);
          nc[nc.n_elem - 1] = 1;

          indices.push_back(emptyset);
        }

        next_c = next_class(nc);
      }else{
        // we are assigning to a current cluster
        nc[new_c]++;
      }

      classes[i] = new_c;
      indices[new_c].insert(i);
    }


    /// split-merge

    if(iteration % 10 == 0){
      // pick two random observations
      // propose merge if they're in different clusters
      // propose split otherwise

      obs = sample_ints(2, n);

      k1  = classes[obs[0]];
      k2  = classes[obs[1]];

      classes_launch = classes;

      /// prepare launch state
      if(k1 == k2){
        // split
        mh = 1;

        // move obs[1] to new_c
        new_c = next_c;
        classes_launch[obs[1]] = new_c;
        // nc_launch would need to be updated but it's not actually used
        // NOTE : next_c needs to be updated
        k2_bis = new_c;

        // set S
        // indices of elements of class k1 except obs[0] and obs[1]
        // it is their class assignments that will be subject to the RGSSs
        // vs = find_minus2(classes_launch, k1, obs[0], obs[1]);
        //
        // we have just moved obs[1] to a different class..
        //
        // nc[k1] wrt classes_launch is one higher, but this is fine. It
        // wouldn't be if it were smaller!
        vs = find_minus1(classes_launch, nc[k1], k1, obs[0]);
      }else{
        // merge
        mh = 3;

        // classes remain the same for elements in obs

        // set S
        // indices of elements of class k1 except obs[0]
        // and of class k2 except obs[1]
        vs = join_cols(
          find_minus1(classes_launch, nc[k1], k1, obs[0]),
          find_minus1(classes_launch, nc[k2], k2, obs[1])
        );

        k2_bis = k2;
      }


      // randomly initialise S
      for(i = 0; i < vs.n_elem; ++i){
        classes_launch[vs[i]] = arma::randu<double>() < 0.5 ? k1 : k2_bis;
      }

      nc_launch = count_classes(classes_launch);


      // restricted Gibbs sampling scans
      lprobs.set_size(2);
      lprobs.fill(-arma::datum::inf);

      // eq. 3.14
      for(i = 0; i < scans; ++i){
        for(j = 0; j < vs.n_elem; ++j){
          // reassign vs[j]
          // remove vs[j], element of S, from counts
          nc_launch[classes_launch[vs[j]]]--;

          lprobs[0] = std::log(nc_launch[k1]) +
            dbbinom_gibbs_old(
              reads,
              depths,
              wreads,
              vs[j],
              find_minus1(classes_launch, nc_launch[k1], k1, vs[j])
            );
          // k2 = k1 or new_c -- use k2_bis
          lprobs[1] = std::log(nc_launch[k2_bis]) +
            dbbinom_gibbs_old(
              reads,
              depths,
              wreads,
              vs[j],
              find_minus1(classes_launch, nc_launch[k2_bis], k2_bis, vs[j])
            );

          classes_launch[vs[j]] = sample_from_two(
            k1,
            k2_bis,
            lnormalise(lprobs, range2, true)
          );
          nc_launch[classes_launch[vs[j]]]++;
        }
      }



      // prepare proposal
      mh_ratio = 0;

      // one final RGSS
      if(k1 == k2){
        // split
        classes_new = classes_launch;
        nc_new = nc_launch;
        
        // one more RGSS from _launch to _new keeping track of transitions
        for(j = 0; j < vs.n_elem; ++j){
          // reassign vs[j]

          // remove vs[j], element of S, from counts
          // we remove the count so that log(nc_new[]) below is correct
          // but this may give the wrong `n` for find_minus1
          nc_new[classes_new[vs[j]]]--;

          // k1 = classes_new.at(obs[0])
          lprobs[0] = std::log(nc_new[k1]) +
            dbbinom_gibbs_old(
              reads,
              depths,
              wreads,
              vs[j],
              find_minus1(classes_new, nc_new[k1] + 1, k1, vs[j])
            );
          // k2 = k1 or new_c -- use k2_bis
          lprobs[1] = std::log(nc_new[k2_bis]) +
            dbbinom_gibbs_old(
              reads,
              depths,
              wreads,
              vs[j],
              find_minus1(classes_new, nc_new[k2_bis] + 1, k2_bis, vs[j])
            );

          lprobs = lnormalise(lprobs, range2, false);
          probs  = arma::exp(lprobs);

          classes_new[vs[j]] = sample_from_two(
            k1,
            k2_bis,
            probs
          );

          new_c = classes_new[vs[j]];

          if(new_c == k1){
            // for split, ratio of proposals is 1 / q(c_new | c)
            mh_ratio -= lprobs[0];
          }else{
            mh_ratio -= lprobs[1];
          }

          nc_new[new_c]++;
        }

        mh_ratio += dbbinom_sm(
            reads,
            depths,
            wreads,
            arma::find(classes_new == k1)
          ) +
          dbbinom_sm(
            reads,
            depths,
            wreads,
            arma::find(classes_new == k2_bis)
          ) -
          dbbinom_sm(
            reads,
            depths,
            wreads,
            arma::find(classes == k1)
          ) +
          lalpha + 
          std::lgamma(nc_new[k1]) +
          std::lgamma(nc_new[k2_bis]) -
          std::lgamma(nc[k1]);
      }else{
        // merge
        nc_new = nc;
        classes_new = classes;

        nc_new[k1] += nc_new[k2];
        nc_new[k2]  = 0;

        // obs[0] is already in k1
        classes_new[obs[1]] = k1;

        // compute the probability of moving from launch state to original state
        for(j = 0; j < vs.n_elem; ++j){
          // and while at it, update the class of the elements in S (`vs`)
          classes_new[vs[j]] = k1;

          // remove vs[j], element of S, from counts
          // we remove the count so that log(nc_new[]) below is correct
          // but this may give the wrong `n` for find_minus1
          // CLASSES_LAUNCH ?!
          nc_launch[classes_launch[vs[j]]]--;

          // k1 = classes_launch.at(obs[0])
          lprobs[0] = std::log(nc_launch[k1]) +
            dbbinom_gibbs_old(
              reads,
              depths,
              wreads,
              vs[j],
              find_minus1(classes_launch, nc_launch[k1] + 1, k1, vs[j])
            );
          // k2 = k1 or new_c -- use k2_bis
          lprobs[1] = std::log(nc_launch[k2_bis]) +
            dbbinom_gibbs_old(
              reads,
              depths,
              wreads,
              vs[j],
              find_minus1(classes_launch, nc_launch[k2_bis] + 1, k2_bis, vs[j])
            );
          // restore: we're not actually changing anything here, just keeping
          // track of the probabilities
          nc_launch[classes_launch[vs[j]]]++;

          lprobs = lnormalise(lprobs, range2, false);

          // CLASSES ?!
          new_c = classes[vs[j]];

          if(new_c == k1){
            mh_ratio += lprobs[0];
          }else{
            mh_ratio += lprobs[1];
          }
        }

        mh_ratio += dbbinom_sm(
            reads,
            depths,
            wreads,
            arma::find(classes_new == k1)
          ) -
          dbbinom_sm(
            reads,
            depths,
            wreads,
            arma::find(classes == k1)
          ) -
          dbbinom_sm(
            reads,
            depths,
            wreads,
            arma::find(classes == k2)
          ) -
          lalpha +
          std::lgamma(nc_new[k1]) -
          std::lgamma(nc[k1]) -
          std::lgamma(nc[k2]);
      }

      if(std::log(arma::randu<double>()) < mh_ratio){
        classes = classes_new;
        nc      = nc_new;
        next_c  = next_class(nc);
        k       = nc.n_elem;
        mh++;
      }
    }

    /// update indices
    // rather than tracking all that happens during the split-merge move,
    // it's easier to update the indices from scratch
    indices.resize(k);
    for(j = 0; j < k; ++j){
      indices[j].clear();
    }
    for(i = 0; i < n; ++i){
      indices[classes[i]].insert(i);
    }


    ll = dbbinom_ll(
      reads,
      depths,
      wreads,
      classes,
      nc
    );

    traces[iteration] = Rcpp::List::create(
      Rcpp::_["classes"] = classes,
      Rcpp::_["k"]       = arma::sum(nc > 0),
      Rcpp::_["mh"]      = mh,
      Rcpp::_["ll"]      = ll,
      Rcpp::_["lp"]      = ll + lprior_c(alpha, nc)
    );

    Rcpp::checkUserInterrupt();
  }

  Rcpp::Rcout << "Clustering done             " << std::endl;

  return traces;
}
